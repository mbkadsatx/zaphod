# Zaphod 3DP Case

A simple bottom case for the Zaphod keyboard.

## Mounting Hardware

The 3dp case is designed to be printed, and then have [M3 threaded inserts](https://www.adafruit.com/product/4256)
inserted from the bottom of the case.

Once inserted the PCB is then secured to the case using m3 x 5mm screws.
